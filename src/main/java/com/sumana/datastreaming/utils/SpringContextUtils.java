package com.sumana.datastreaming.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SpringContextUtils implements ApplicationContextAware {
	private static ApplicationContext context;
	
	public static <T> T getBean(String fcn) throws BeansException, ClassNotFoundException {
		return (T) context.getBean(Class.forName(fcn));
	}
	
	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		SpringContextUtils.context = context;
	}

}
