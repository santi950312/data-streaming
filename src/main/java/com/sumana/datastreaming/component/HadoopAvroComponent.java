package com.sumana.datastreaming.component;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Arrays;

import javax.annotation.PreDestroy;
import javax.jws.Oneway;

import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.mapred.AvroInputFormat;
import org.apache.avro.mapred.AvroJob;
import org.apache.avro.mapred.AvroOutputFormat;
import org.apache.avro.mapred.AvroWrapper;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.mapred.JobConf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.hadoop.HdfsSinks;
import com.hazelcast.jet.hadoop.HdfsSources;
import com.hazelcast.jet.impl.util.Util;
import com.hazelcast.jet.pipeline.Pipeline;
import com.sumana.datastreaming.model.Person;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

@Component
public class HadoopAvroComponent {
	private static final String input_path = parentDirectory() + "/hdfs-datastream-input";
	private static final String output_path = parentDirectory() + "/hdfs-datastream-output";
	
	private static DataFileWriter<Person> fileWriter = new DataFileWriter<>(new GenericDatumWriter<>(Person.SCHEMA));
	
	@Autowired
	private JetInstance jetInstance;
	
	public void executeJob(Pipeline pipeline) {
		jetInstance.newJob(pipeline).join();
	}
	
	public Single<Pipeline> buildHadoopPipeline(JobConf conf) {
		return Single.just(conf).subscribeOn(Schedulers.io()).flatMap(cfg -> {
			Pipeline pipeline = Pipeline.create();
			try {
				pipeline.drawFrom(HdfsSources.<AvroWrapper<Person>, NullWritable>hdfs(cfg))
						.peek(entry -> entry.getKey().datum().toString())
						.drainTo(HdfsSinks.hdfs(cfg));
			} catch (Exception e) {
				return Single.error(e);
			}
			return Single.just(pipeline);
		});
	}
	
	public JobConf getjobConf() throws IOException {
		Path inputPath = new Path(input_path);
        Path outputPath = new Path(output_path);
        
        FileSystem.get(new Configuration()).delete(outputPath, true);

        JobConf jobConfig = new JobConf();
        jobConfig.setInputFormat(AvroInputFormat.class);
        jobConfig.setOutputFormat(AvroOutputFormat.class);
        AvroOutputFormat.setOutputPath(jobConfig, outputPath);
        AvroInputFormat.addInputPath(jobConfig, inputPath);
        jobConfig.set(AvroJob.OUTPUT_SCHEMA, Person.SCHEMA.toString());
        jobConfig.set(AvroJob.INPUT_SCHEMA, Person.SCHEMA.toString());
        
        return jobConfig;
	}

	public void createAvroFile(String fileName) throws IOException {
		Path path = new Path(input_path);
		FileSystem fs = FileSystem.get(new Configuration());
		fileWriter.create(Person.SCHEMA, fs.create(new Path(path, fileName + ".avro")));
		fs.close();
	}
	
	public void appendToAvroFile(Person... persons) {
		Arrays.asList(persons).forEach(person -> Util.uncheckRun(() -> fileWriter.append(person)));
	}

	private static String parentDirectory() {
		String path = HadoopAvroComponent.class.getClassLoader().getResource("").getPath();
		return Paths.get(path).getParent().getParent().toString();
	}
	
	@PreDestroy
	public void close() throws IOException {
		fileWriter.close();
	}
}
