package com.sumana.datastreaming.configuration;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import com.hazelcast.config.Config;
import com.hazelcast.config.GroupConfig;
import com.hazelcast.config.ListenerConfig;
import com.hazelcast.config.XmlConfigBuilder;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.jet.Jet;
import com.hazelcast.jet.JetInstance;
import com.hazelcast.jet.config.JetConfig;

@Configuration
public class HazelcastConfiguration {
	@Value("#{'${hazelcast.cluster.host:localhost}'.split(',')}")
	private List<String> hazelcastClusterNodes;

	@Value("${hazelcast.cluster.interface:127.0.0.*}")
	private String hazelcastInterface;

	@Value("${hazelcast.node.lite:false}")
	private Boolean isLite;

	@Value("${hazelcast.group.name:dev}")
	private String groupName;

	@Value("${hazelcast.membership-listener:false}")
	private boolean activeClusterListener;

	@Bean
	public JetInstance hazelcastInstance() {
		Config config = new XmlConfigBuilder(this.getClass().getResourceAsStream("/hazelcast.xml")).build();
		config.setLiteMember(isLite);
		config.setGroupConfig(new GroupConfig().setName(groupName));

		if (Optional.ofNullable(hazelcastClusterNodes).isPresent()) {
			if (hazelcastClusterNodes != null && hazelcastClusterNodes.size() > 0) {
				config.getNetworkConfig().getJoin().getTcpIpConfig().setEnabled(true);
				for (String member : hazelcastClusterNodes) {
					config.getNetworkConfig().getJoin().getTcpIpConfig().addMember(member.trim());
				}
			}
		}

		if (hazelcastInterface != null && hazelcastInterface.length() > 0) {
			config.getNetworkConfig().getInterfaces().clear();
			config.getNetworkConfig().getInterfaces().addInterface(hazelcastInterface).setEnabled(true);
		}
		
		//Creating a clustered hz jet instance -> we can get the hz instance from that jet instance anywhere
		JetInstance jetInstance = Jet.newJetInstance(new JetConfig().setHazelcastConfig(config));
//		HazelcastInstance hazelcastInstance = Hazelcast.newHazelcastInstance(config);
		return jetInstance;
	}

}
