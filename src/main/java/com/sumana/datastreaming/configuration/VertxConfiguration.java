package com.sumana.datastreaming.configuration;

import com.hazelcast.jet.JetInstance;

import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.spi.cluster.hazelcast.HazelcastClusterManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@Configuration
public class VertxConfiguration {

    private static final Logger logger = LoggerFactory.getLogger(VertxConfiguration.class);

    private Vertx vertx;

    @Autowired
    private JetInstance jetInstance;

    @Bean(destroyMethod = "")
    public Vertx vertx() {
        return vertx;
    }

    @PostConstruct
    public void init() throws UnknownHostException, InterruptedException, ExecutionException {
        VertxOptions options = new VertxOptions();
        options.setMaxEventLoopExecuteTime(Long.MAX_VALUE);
        options.setClustered(true);
        options.setClusterManager(new HazelcastClusterManager(jetInstance.getHazelcastInstance()));
        options.setClusterHost(Inet4Address.getLocalHost().getHostAddress());

        CompletableFuture<Vertx> future = new CompletableFuture<Vertx>();
        Vertx.clusteredVertx(options, ar -> {
            if (ar.succeeded()) {
                logger.debug("Vertx instance declared succesful");
                future.complete(ar.result());
            } else {
                future.completeExceptionally(ar.cause());
            }
        });

        vertx = future.get();

    }

    @PreDestroy
    public void close() throws ExecutionException, InterruptedException {
        CompletableFuture<Void> future = new CompletableFuture<>();
        vertx.close(ar -> future.complete(null));
        future.get();
    }
}
