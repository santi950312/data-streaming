package com.sumana.datastreaming.service.processors;

import io.vertx.core.json.JsonObject;

public interface DataProcessorService {
	public JsonObject processData();
}
