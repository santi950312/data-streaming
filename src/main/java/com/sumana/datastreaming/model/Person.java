package com.sumana.datastreaming.model;

import java.io.Serializable;

import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.avro.specific.SpecificRecord;

import io.vertx.codegen.annotations.DataObject;
import io.vertx.core.json.JsonObject;

@DataObject(generateConverter = true)
public class Person implements Serializable, SpecificRecord {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4937751256657345745L;

	public static final Schema SCHEMA = SchemaBuilder.record(Person.class.getSimpleName())
			.namespace(Person.class.getPackage().getName()).fields().name("age").type().intType().noDefault()
			.name("gender").type().stringType().noDefault().name("ts").type().longType().noDefault().endRecord();

	private Integer age;
	private String gender;
	private Long ts;

	public Person() {
		// TODO Auto-generated constructor stub
	}

	public Person(JsonObject json) {
		PersonConverter.fromJson(json, this);
	}

	public JsonObject toJson() {
		JsonObject json = new JsonObject();
		PersonConverter.toJson(this, json);
		return json;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Long getTs() {
		return ts;
	}

	public void setTs(Long ts) {
		this.ts = ts;
	}

	@Override
	public void put(int i, Object v) {
		switch (i) {
		case 0:
			this.age = (Integer) v;
			break;
		
		case 1:
			this.gender = v.toString();
			break;

		case 2:
			this.ts = (Long) v;
			break;

		default:
			throw new IllegalArgumentException();
		}
	}

	@Override
	public Object get(int i) {
		switch (i) {
		case 0:
			return age;
			
		case 1:
			return gender;
			
		case 2:
			return ts;

		default:
			throw new IllegalArgumentException();
		}
	}

	@Override
	public Schema getSchema() {
		return SCHEMA;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "avro.Person{"
				+ "age=" + age
				+ ",gender='" + gender + "'"
				+ ",ts=" + ts
				+ "}";
	}
}
