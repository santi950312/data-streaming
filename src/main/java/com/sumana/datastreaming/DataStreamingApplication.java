package com.sumana.datastreaming;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import com.sumana.datastreaming.utils.SpringContextUtils;
import com.sumana.datastreaming.verticle.DataProcessorVerticle;
import com.sumana.datastreaming.verticle.PublisherVerticle;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.Verticle;
import io.vertx.core.Vertx;

@SpringBootApplication
@EnableAutoConfiguration(exclude = org.springframework.boot.autoconfigure.hazelcast.HazelcastAutoConfiguration.class)
public class DataStreamingApplication {
	
	@Autowired
	private Vertx vertx;
	
    public static void main(String[] args) {
        SpringApplication.run(DataStreamingApplication.class, args);
    }
    
    @EventListener
    private void initVerticles(ApplicationReadyEvent evt) throws BeansException, ClassNotFoundException {
    	//Setting multiple instanecs
    	DeploymentOptions options = new DeploymentOptions();
    	options.setInstances(1).setWorker(true).setMultiThreaded(true);
    	//data processor verticle -> listening for incoming data and dispatching to the pipeline
    	vertx.deployVerticle(SpringContextUtils.<Verticle>getBean(DataProcessorVerticle.class.getName()), options);
    	//This pubisherVerticle is just a application that sends messages every 10 seconds to emulate some transactions on the platform
    	vertx.deployVerticle(SpringContextUtils.<Verticle>getBean(PublisherVerticle.class.getName()), options);

    }
}

