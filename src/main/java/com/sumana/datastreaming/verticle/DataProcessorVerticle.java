package com.sumana.datastreaming.verticle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.sumana.datastreaming.component.HadoopAvroComponent;
import com.sumana.datastreaming.model.Person;

import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.AbstractVerticle;

@Component
@Scope("prototype")
public class DataProcessorVerticle extends AbstractVerticle {
	
	private static final Logger log = LoggerFactory.getLogger(DataProcessorVerticle.class);
	
	@Autowired
	private HadoopAvroComponent avroComponent;

	@Override
	public void start(Future<Void> startFuture) throws Exception {
		avroComponent.createAvroFile("persons");
		vertx.eventBus().<JsonObject>consumer("DataProcessing", message -> {
			Person person = new Person(message.body());
			try {
				avroComponent.buildHadoopPipeline(avroComponent.getjobConf())
						.subscribe(pipeline -> avroComponent.executeJob(pipeline));
			} catch (Exception e) {
				log.error("I/O Error on hdfs operation", e);
			}

		}).completionHandler(startFuture);
	}
}
