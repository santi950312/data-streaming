package com.sumana.datastreaming.verticle;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import io.vertx.core.Future;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.AbstractVerticle;

@Component
@Scope("prototype")
public class PublisherVerticle extends AbstractVerticle {
	
	private static final Logger log = LoggerFactory.getLogger(PublisherVerticle.class);

    @Override
    public void start(Future<Void> startFuture) throws Exception {
    	
    	Random random = new Random();
    	vertx.setPeriodic(10000, l -> {
        	log.debug("Periodic Message sended");
        	JsonObject data = new JsonObject();
        	data.put("age", random.nextInt(60))
        	.put("gender", random.nextInt(1) == 0 ? "male" : "female")
        	.put("ts", LocalDateTime.now().atZone(ZoneId.of("America/Bogota")).toInstant().toEpochMilli());
        	vertx.eventBus().send("DataProcessing", data);
        });

        startFuture.complete();
    }
}
